{ mkDerivation, base, bytestring, criterion, deepseq, ghc-prim
, html-entities, lib, tasty, tasty-hunit, text
, unordered-containers, xeno, xml, xmlbf, aeson
}:
mkDerivation {
  pname = "xmlbf-xeno";
  version = "0.2.2";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring html-entities text unordered-containers xeno xmlbf aeson
  ];
  testHaskellDepends = [ base tasty tasty-hunit xmlbf ];
  benchmarkHaskellDepends = [
    base bytestring criterion deepseq ghc-prim xml
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "xeno backend support for the xmlbf library";
  license = lib.licenses.asl20;
  doBenchmark = true;
}
